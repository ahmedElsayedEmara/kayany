$(function () {

  "use strict";

  var getDirection = $('body').css('direction');


  // show and hidden password 
  var btnShowPassword = $('.btn-show-password'),
    myPasswordInput = $('.my-password-input');

  btnShowPassword.click(function () {
    var passwordElement = $(this).siblings(myPasswordInput);
    if (passwordElement.attr("type") == "password") {
      passwordElement.attr("type", "text");
      $(this).addClass('fa-eye-slash');
    } else {
      passwordElement.attr("type", "password");
      $(this).removeClass('fa-eye-slash');

    }
  });


  // text editor summernote
  $('.summernote').summernote({
    height: 200,

  });

  // lightbox
  lightbox.option({
    'fadeDuration': 300,
    'showImageNumberLabel': false,
    "maxWidth": 800,
    "maxHeight": 600
  })

  // Calendar
  $('.dataCalendar').datepicker({
    todayBtn: "linked",
    keyboardNavigation: false,
    forceParse: false,
    calendarWeeks: true,
    autoclose: true
  });

  // drag and drop table 
  $(".sortable").sortable();

  // show and hide add service 
  $('.add-service').click(function () {
    $('.add-service-group').slideDown()
  })
  $('.add-service-group .btn-orange,.add-service-group .btn-cancel').click(function () {
    $('.add-service-group').slideUp()
  })

  // show and hide add sub category 
  $('.add-sub-category').click(function () {
    $('.add-subcategory-group').slideDown()
  })
  $('.add-subcategory-group .btn-orange,.add-subcategory-group .btn-cancel').click(function () {
    $('.add-subcategory-group').slideUp()
  })




});
// loading
window.onload = function () {
  $('.loader').delay(200).fadeOut(500);

}