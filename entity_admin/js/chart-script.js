var ctx = document.getElementById('myChart');
var myChartType = 'bar';
var myChartlabel = '';
var myChartLabels = ['الاختيار 1', 'الاختيار 2', 'الاختيار 3', 'الاختيار 4'];
var myChartData = [200, 100, 30, 50];
var myChartBackgroundColor = [
  '#ffa800',
  '#000000',
  '#e70000',
  '#9a9a9a'
];
var myChartBorderColor = [
  '#ffa800',
  '#000000',
  '#e70000',
  '#9a9a9a',
];
if (ctx) {
  var myChart = new Chart(ctx.getContext('2d'),
    chart(myChartType, myChartLabels, myChartlabel, myChartData, myChartBackgroundColor, myChartBorderColor)
  );
}

// end 

var ctxThree = document.getElementById('myChartThree');
var myChartType = 'bar';
var myChartlabel = '';
var myChartLabels = ['الاختيار 1', 'الاختيار 2'];
var myChartData = [100, 195];
var myChartBackgroundColor = [
  '#ffa800',
  '#e70000',
];
var myChartBorderColor = [
  '#ffa800',
  '#e70000',
];

if (ctxThree) {
  var ctxThree = new Chart(ctxThree.getContext('2d'),
    chart(myChartType, myChartLabels, myChartlabel, myChartData, myChartBackgroundColor, myChartBorderColor)
  );
}

// end 

var dashboardSerivce = document.getElementById('dashboardSerivce');
var myChartType = 'bar';
var myChartlabel = '';
var myChartLabels = ['اسم الجهة', 'اسم الجهة', 'اسم الجهة', 'اسم الجهة', 'اسم الجهة'];
var myChartData = [100, 195, 30, 42, 10];
var myChartBackgroundColor = [
  '#ffa800',
  '#ffa800',
  '#ffa800',
  '#ffa800',
  '#ffa800',
];
var myChartBorderColor = [
  '#ffa800',
  '#ffa800',
  '#ffa800',
  '#ffa800',
  '#ffa800',
];
if (dashboardSerivce) {
  var dashboardSerivce = new Chart(dashboardSerivce.getContext('2d'),
    chart(myChartType, myChartLabels, myChartlabel, myChartData, myChartBackgroundColor, myChartBorderColor)
  );
}

// end 
var dashboardEvent = document.getElementById('dashboardEvent');
var myChartType = 'bar';
var myChartlabel = '';
var myChartLabels = ['اسم الجهة', 'اسم الجهة', 'اسم الجهة', 'اسم الجهة', 'اسم الجهة', 'اسم الجهة', 'اسم الجهة', 'اسم الجهة'];
var myChartData = [100, 195, 30, 42, 10, 45, 23, 21];
var myChartBackgroundColor = [
  '#e70000',
  '#e70000',
  '#e70000',
  '#e70000',
  '#e70000',
  '#e70000',
  '#e70000',
  '#e70000'
];
var myChartBorderColor = [
  '#e70000',
  '#e70000',
  '#e70000',
  '#e70000',
  '#e70000',
  '#e70000',
  '#e70000',
  '#e70000'
];
if (dashboardEvent) {
  var dashboardEvent = new Chart(dashboardEvent.getContext('2d'),
    chart(myChartType, myChartLabels, myChartlabel, myChartData, myChartBackgroundColor, myChartBorderColor)
  );
}

// end 

var dashboardUsers = document.getElementById('dashboardUsers');
var myChartType = 'bar';
var myChartlabel = '';
var myChartLabels = ['يناير', 'فبراير', 'مارس', 'ابريل', 'مايو', 'يونيو', 'يوليو', 'اغسطس', 'سبتمبر', 'اكتوبر', 'نوفمبر', 'ديسمبر',];
var myChartData = [100, 195, 30, 42, 10, 45, 23, 21, 54, 44, 12, 34];
var myChartBackgroundColor = [
  '#e70000',
  '#e70000',
  '#e70000',
  '#e70000',
  '#e70000',
  '#e70000',
  '#e70000',
  '#e70000',
  '#e70000',
  '#e70000',
  '#e70000',
  '#e70000'
];
var myChartBorderColor = [
  '#e70000',
  '#e70000',
  '#e70000',
  '#e70000',
  '#e70000',
  '#e70000',
  '#e70000',
  '#e70000',
  '#e70000',
  '#e70000',
  '#e70000',
  '#e70000'
];
if (dashboardUsers) {
  var dashboardUsers = new Chart(dashboardUsers.getContext('2d'),
    chart(myChartType, myChartLabels, myChartlabel, myChartData, myChartBackgroundColor, myChartBorderColor)
  );
}

// end 

var dashboardEntityServices = document.getElementById('dashboardEntityServices');
var myChartType = 'bar';
var myChartlabel = '';
var myChartLabels = ['يناير', 'فبراير', 'مارس', 'ابريل', 'مايو', 'يونيو', 'يوليو', 'اغسطس', 'سبتمبر', 'اكتوبر', 'نوفمبر', 'ديسمبر',];
var myChartData = [100, 195, 30, 42, 10, 45, 23, 21, 54, 44, 12, 34];
var myChartBackgroundColor = [
  '#ffa800',
  '#ffa800',
  '#ffa800',
  '#ffa800',
  '#ffa800',
  '#ffa800',
  '#ffa800',
  '#ffa800',
  '#ffa800',
  '#ffa800',
  '#ffa800',
  '#ffa800'
];
var myChartBorderColor = [
  '#ffa800',
  '#ffa800',
  '#ffa800',
  '#ffa800',
  '#ffa800',
  '#ffa800',
  '#ffa800',
  '#ffa800',
  '#ffa800',
  '#ffa800',
  '#ffa800',
  '#ffa800'
];
if (dashboardEntityServices) {
  var dashboardEntityServices = new Chart(dashboardEntityServices.getContext('2d'),
    chart(myChartType, myChartLabels, myChartlabel, myChartData, myChartBackgroundColor, myChartBorderColor)
  );
}

// end 

var dashboardEntityEvents = document.getElementById('dashboardEntityEvents');
var myChartType = 'bar';
var myChartlabel = '';
var myChartLabels = ['يناير', 'فبراير', 'مارس', 'ابريل', 'مايو', 'يونيو', 'يوليو', 'اغسطس', 'سبتمبر', 'اكتوبر', 'نوفمبر', 'ديسمبر',];
var myChartData = [100, 195, 30, 42, 10, 45, 23, 21, 54, 44, 12, 34];
var myChartBackgroundColor = [
  '#ffa800',
  '#ffa800',
  '#ffa800',
  '#ffa800',
  '#ffa800',
  '#ffa800',
  '#ffa800',
  '#ffa800',
  '#ffa800',
  '#ffa800',
  '#ffa800',
  '#ffa800'
];
var myChartBorderColor = [
  '#ffa800',
  '#ffa800',
  '#ffa800',
  '#ffa800',
  '#ffa800',
  '#ffa800',
  '#ffa800',
  '#ffa800',
  '#ffa800',
  '#ffa800',
  '#ffa800',
  '#ffa800'
];
if (dashboardEntityEvents) {
  var dashboardEntityEvents = new Chart(dashboardEntityEvents.getContext('2d'),
    chart(myChartType, myChartLabels, myChartlabel, myChartData, myChartBackgroundColor, myChartBorderColor)
  );
}

// end 

var dashboardEntityUsers = document.getElementById('dashboardEntityUsers');
var myChartType = 'bar';
var myChartlabel = '';
var myChartLabels = ['اسم الجهة', 'اسم الجهة', 'اسم الجهة', 'اسم الجهة', 'اسم الجهة', 'اسم الجهة', 'اسم الجهة', 'اسم الجهة'];
var myChartData = [100, 195, 30, 42, 10, 45, 23, 21];
var myChartBackgroundColor = [
  '#e70000',
  '#e70000',
  '#e70000',
  '#e70000',
  '#e70000',
  '#e70000',
  '#e70000',
  '#e70000'
];
var myChartBorderColor = [
  '#e70000',
  '#e70000',
  '#e70000',
  '#e70000',
  '#e70000',
  '#e70000',
  '#e70000',
  '#e70000'
];
if (dashboardEntityUsers) {
  var dashboardEntityUsers = new Chart(dashboardEntityUsers.getContext('2d'),
    chart(myChartType, myChartLabels, myChartlabel, myChartData, myChartBackgroundColor, myChartBorderColor)
  );
}

// end 
var theHelp = Chart.helpers;

var dashboardPie = document.getElementById('dashboardPie');
var myChartType = 'doughnut';
var myChartlabel = '';
var myChartLabels = ['مستخدمين ذكور', 'مستخدمين إناث'];
var myChartData = [1100, 695];
var myChartBackgroundColor = [
  '#e70000',
  '#ffa800',
];
var myChartBorderColor = [
  '#e70000',
  '#ffa800',
];
if (dashboardPie) {
  var dashboardPie = new Chart(dashboardPie,
    {
      type: myChartType,
      data: {
        labels: myChartLabels,
        datasets: [{
          label: ' ',
          data: myChartData,
          backgroundColor: myChartBackgroundColor,
          borderColor: myChartBorderColor,
          borderWidth: 1,
          barThickness: 15,

        }]
      },
      options: {
        cutoutPercentage: 80,
        responsive: true,
        legend: {
          position: 'right',
          rtl: true,
          labels: {
            fontFamily: "cairo",
            usePointStyle: true,
            padding: 20,
            generateLabels: function (chart) {
              var data = chart.data;
              if (data.labels.length && data.datasets.length) {
                return data.labels.map(function (label, i) {
                  var meta = chart.getDatasetMeta(0);
                  var ds = data.datasets[0];
                  var arc = meta.data[i];
                  var custom = arc && arc.custom || {};
                  var getValueAtIndexOrDefault = theHelp.getValueAtIndexOrDefault;
                  var arcOpts = chart.options.elements.arc;
                  var fill = custom.backgroundColor ? custom.backgroundColor : getValueAtIndexOrDefault(ds.backgroundColor, i, arcOpts.backgroundColor);
                  var stroke = custom.borderColor ? custom.borderColor : getValueAtIndexOrDefault(ds.borderColor, i, arcOpts.borderColor);
                  var bw = custom.borderWidth ? custom.borderWidth : getValueAtIndexOrDefault(ds.borderWidth, i, arcOpts.borderWidth);
                  return {
                    // And finally : 
                    text: label + "    " + ds.data[i],
                    fillStyle: fill,
                    strokeStyle: stroke,
                    lineWidth: bw,
                    hidden: isNaN(ds.data[i]) || meta.data[i].hidden,
                    index: i
                  };
                });
              }
              return [];
            }
          }
        },
        tooltips: {
          mode: "dataset"
        },
        events: false,
        animation: {
          duration: 500,
          easing: "easeOutQuart",
          onComplete: function () {
            var chartInstance = this.chart,
              ctx = chartInstance.ctx;
            ctx.font = Chart.helpers.fontString(Chart.defaults.global.defaultFontFamily, 'tahoma', Chart.defaults.global.defaultFontSize, 25, Chart.defaults.global.defaultFontFamily);
            ctx.textAlign = 'center';
            ctx.font = '25px cairo';

            this.data.datasets.forEach(function (dataset, i) {
              var meta = chartInstance.controller.getDatasetMeta(i);
              meta.data.forEach(function (bar) {
                var data = dataset.data.reduce((num1, num2) => num1 + num2, 0);
                ctx.fillText(data, bar._model.x, bar._model.y + 10);
              });
            });
          }
        }
      }
    }
  );
}

// end 

// ********************// 
// ********************// 
//  chart generator
function chart(type, labels, label, data, backgroundColor, borderColor) {
  return {
    type: type,
    responsive: true,
    data: {
      labels: labels,
      datasets: [{
        label: label,
        data: data,
        backgroundColor: backgroundColor,
        borderColor: borderColor,
        borderWidth: 1,
        barThickness: 25,
      }]
    },
    options: {
      layout: {
        padding: {
          left: 0,
          right: 0,
          top: 20,
          bottom: 20
        }
      },
      scales: {
        yAxes: [{
          ticks: {
            beginAtZero: true
          }
        }],
        xAxes: [{
          ticks: {
            fontFamily: "cairo",

          }
        }],
      },
      legend: {
        display: false,
      },
      "hover": {
        "animationDuration": 0
      },
      "animation": {
        "onComplete": function () {
          var chartInstance = this.chart,
            ctx = chartInstance.ctx;

          ctx.font = Chart.helpers.fontString(Chart.defaults.global.defaultFontSize, Chart.defaults.global.defaultFontStyle, Chart.defaults.global.defaultFontFamily);
          ctx.textAlign = 'center';
          ctx.textBaseline = 'bottom';

          this.data.datasets.forEach(function (dataset, i) {
            var meta = chartInstance.controller.getDatasetMeta(i);
            meta.data.forEach(function (bar, index) {
              var data = dataset.data[index];
              ctx.fillText(data, bar._model.x, bar._model.y - 5);
            });
          });
        }
      },
      title: {
        display: false,
        text: ''
      },
    }
  }
}